use scraper::ElementRef;
use scraper::Html;
use scraper::Selector;

pub fn get_lyrics(artist: &str, song: &str) -> Option<Vec<String>> {
    let client = reqwest::blocking::Client::new();
    let url = format!(
        "https://www.azlyrics.com/lyrics/{}/{}.html",
        format_az_metadata(artist),
        format_az_metadata(song)
    );
    let resp = client.get(&url).send().unwrap().text().unwrap();

    let document = Html::parse_document(resp.as_str());
    let body_selector = Selector::parse("body").unwrap();

    let body = document.select(&body_selector).next().unwrap();

    let div = match find_div_child(&body, "container main-page") {
        Some(e) => e,
        None => return None,
    };
    let div = match find_div_child(&div, "row") {
        Some(e) => e,
        None => return None,
    };
    let div = match find_div_child(&div, "col-xs-12 col-lg-8 text-center") {
        Some(e) => e,
        None => return None,
    };

    let div_selector = Selector::parse("div").unwrap();

    for element in div.select(&div_selector) {
        match element.value().attr("class") {
            Some(_) => continue,
            None => {
                let mut lyrics: Vec<String> = Vec::new();
                let lines = element.text().collect::<Vec<_>>();
                let mut start = true;

                for i in 0..lines.len() {
                    let line = lines[i].to_owned();

                    if start == true {
                        if line.trim().is_empty() {
                            continue;
                        } else {
                            start = false;
                        }
                    }

                    if !(lines[i].trim().is_empty()
                        && i + 1 < lines.len()
                        && lines[i + 1].trim().is_empty())
                    {
                        if !line.contains("freestar.config") {
                            lyrics.push(line.trim().to_owned());
                        }
                    }
                }

                return Some(lyrics);
            }
        }
    }

    return None;
}

fn find_div_child<'a>(fragment: &'a ElementRef, class: &str) -> Option<ElementRef<'a>> {
    let div_selector = Selector::parse("div").unwrap();

    for element in fragment.select(&div_selector) {
        match element.value().attr("class") {
            Some(a) => {
                if a.eq(class) {
                    return Some(element);
                }
            }
            None => continue,
        }
    }

    return None;
}

fn format_az_metadata(dat: &str) -> String {
    let mut result = String::new();
    let mut open_delim: bool = false;

    for word in dat.split_whitespace() {
        if open_delim == true {
            if word.ends_with(")") {
                open_delim = false;
            }
        } else {
            if word.starts_with("(feat") || word.starts_with("(ft") {
                open_delim = true;
            } else {
                result.push_str(word);
                result.push_str(" ");
            }
        }
    }
    result
        .trim()
        .to_lowercase()
        .chars()
        .filter(|c| c.is_ascii_alphanumeric())
        .collect()
}
